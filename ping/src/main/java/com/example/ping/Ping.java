package com.example.ping;

import com.example.frameworkworkag0906.SimpleButton;
import com.example.frameworkworkag0906.SimplePlugin;
import com.example.frameworkworkag0906.SimpleApplication;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.List;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;


public class Ping extends SimplePlugin {
    private boolean reachable;
    private Process process;
    private String result;

    private void reset(){
        getTextField().setText("127.0.0.1");
    }

    private void alert(String title, String m1) {
        {
            Stage stage = new Stage();
            stage.setTitle(title);
            Label label = new Label(m1);
            Scene scene = new Scene(label, 500, 300);
            stage.setScene(scene);
            stage.show();
        }
    }

    public String getResults(Process process) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line = "";
        result = "";
        while ((line = reader.readLine()) != null) {
            result += line;
            result += "\n";
        }
        return result;
    }

    private void ping(){
        try {
            InetAddress address = InetAddress.getByName(getTextField().getText());
        } catch (Exception e) {
            alert("Check not passed","Input must be IPv4 adress! \nShould be 1.1.1.1 - 255.255.255.255");
            reset();
            return;
        }
        try {
            String command = "ping -c 3 " + getTextField().getText();
            this.process = Runtime.getRuntime().exec(command);
        } catch (Exception e) {
            e.printStackTrace();
        }
        try {
            this.result = getResults(this.process);
        } catch (Exception e){
            e.printStackTrace();
        }
        alert("Ping request", result);
        reset();
    }

    @Override
    public String getTitle() {
        return "Ping";
    }

    @Override
    public void initTextField() {
        getTextField().setText("127.0.0.1");
    }

    @Override
    public List<SimpleButton> getButtons() {
        ArrayList<SimpleButton> buttons = new ArrayList<>();
        buttons.add(new SimpleButton("ping", (e) -> ping()));
        return buttons;
    }

    public static void main(String[] args) {
        SimpleApplication.setPlugin(new Ping());
        SimpleApplication.launch(SimpleApplication.class, args);
    }
}
