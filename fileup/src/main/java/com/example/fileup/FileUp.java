package com.example.fileup;

import com.example.frameworkworkag0906.SimpleApplication;
import com.example.frameworkworkag0906.SimpleButton;
import com.example.frameworkworkag0906.SimplePlugin;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class FileUp extends SimplePlugin {
    private final String default_text = "user@target_ip:target_path";
    File file_to_upload;
    private String address = "";
    private String user = "";
    private String target_path = "";
    private String result = "";
    private String command;

    private void reset(){
        getTextField().setText(default_text);
    }

    private void alert(String title, String m1) {
        {
            Stage stage = new Stage();
            stage.setTitle(title);
            Label label = new Label(m1);
            Scene scene = new Scene(label, 700, 300);
            stage.setScene(scene);
            stage.show();
        }
    }

    public void getResults(Process process) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
        String line;
        while ((line = reader.readLine()) != null) {
            System.out.println(line);
            this.result += line;
            this.result += "\n";
        }
    }

    private void split_target(){
        try {
            String raw_text = getTextField().getText();
            String[] str_array = raw_text.split("@");
            this.user = str_array[0];
            String[] str_Array = str_array[1].split(":");
            this.address = str_Array[0];
            this.target_path = str_Array[1];
            System.out.println("user = " + user);
            System.out.println("address = " + address);
            System.out.println("target_path = " + target_path);
        } catch (Exception e){
            alert("Fail", "No valid user-address-path string!");
        }
    }

    private void selectFile(){
        Stage stage = new Stage();
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        file_to_upload = fileChooser.showOpenDialog(stage);
        //System.out.println(file_to_upload);
    }

    private void upload(){
        Process process;
        split_target();
        try{
            file_to_upload.canRead();
        } catch(Exception e) {
            alert("Fail", "You have to select a file to upload first!");
            reset();
            return;
        }
        try{
            InetAddress addr = InetAddress.getByName(this.address);
        } catch (Exception e) {
            alert("Fail", "You have to give a IPv4 address");
            reset();
            return;
        }
        try {
            this.command = "scp " + file_to_upload.toString() + " " + this.user + "@" + this.address + ":" + this.target_path;
            System.out.println(command);
            process = Runtime.getRuntime().exec(command);
        } catch (Exception e) {
            e.printStackTrace();
            reset();
            return;
        }
        try {
            getResults(process);
        } catch (Exception e){
            e.printStackTrace();
            reset();
            return;
        }
        if (this.result == ""){
            alert("Upload status unknown", "You used:\n" + this.command +
                    "\n\nIf you see this I wasn't able to handle the result of exec.\n" +
                    "But if you made no typo and have no password on the target \n" +
                    "machine (haha), the file should have been uploaded.\n" +
                    "It could also bee, that you use Windows or apple.... \n" +
                    "Plz use a real OS instead!\n" +
                    "\nWhy can't we just use C++ here? ");
        }
        else {
            alert("Upload status", this.result);
        }
        reset();
    }

    @Override
    public String getTitle() {
        return "File Uploader";
    }

    @Override
    public void initTextField() {
        getTextField().setText(default_text);
        getTextField().setPrefColumnCount(25);
    }

    @Override
    public List<SimpleButton> getButtons() {
        ArrayList<SimpleButton> buttons = new ArrayList<>();
        buttons.add(new SimpleButton("Select File", (e) -> selectFile()));
        buttons.add(new SimpleButton("Upload", (e) -> upload()));
        return buttons;
    }

    public static void main(String[] args) {
        SimpleApplication.setPlugin(new FileUp());
        SimpleApplication.launch(SimpleApplication.class, args);
    }
}